export const utils = {
  //轻提示
  toast(title = '数据加载失败!', icon = 'none') {
    return uni.showToast({
      title: title,
      icon: icon,
      mask: true //蒙层 不允许点击
    })
  }
}
//挂载到全局对象uni身上
uni.utils = utils