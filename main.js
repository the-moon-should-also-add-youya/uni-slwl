import App from './App'
import '@/utils/utils.js'
// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false

App.mpType = 'app'
const app = new Vue({
  ...App,
})
app.$mount()
// #endif

// #ifdef VUE3
import { createPinia } from 'pinia'
import { createSSRApp } from 'vue'
// import piniaPluginPersistedState from 'pinia-plugin-persistedstate'
import { createPersistedState } from 'pinia-plugin-persistedstate'
const piniaPluginPersistedState = createPersistedState({
  //自定义存储的key
  key: id => `__persisted__${id}`,
  //自定义存储模块
  storage: {
    getItem: (key) => {
      return uni.getStorageSync('key')
    },
    setItem: (key, value) => {
      uni.setStorageSync(key, value)
    }
  }
})
export function createApp() {
  const app = createSSRApp(App)
  //创建一个pinia实例
  const pinia = createPinia()
  //pinia注册持久化插件
  // pinia.use(piniaPluginPersistedState())
  pinia.use(piniaPluginPersistedState)
  //在应用内注册pinia
  app.use(pinia)
  return {
    app,
  }
}
// #endif