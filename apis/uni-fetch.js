// apis/uni-fetch.js

// 导入安装好的 uni-app-fetch 模块
import { createUniFetch } from 'uni-app-fetch'
import { useUserStore } from '@/stores/user.js'
//userKey
export const userKey = "__persisted__user"
//tabbar页面
const tabbarPagePaths = [
  '/pages/task/index',
  '/pages/my/index',
  '/pages/message/index'
]
// 配置符合自身业务的请求对象
export const uniFetch = createUniFetch({
  loading: { title: '正在加载...' },
  baseURL: 'https://slwl-api.itheima.net',
  intercept: {
    // 请求拦截器
    request(options) {
      // 后续补充实际逻辑
      //默认头信息
      // console.log(options.header, 'header1');
      const userStore = useUserStore()
      const tokenData = userStore.token
      // console.log(tokenData, 'tokendata');

      // console.log(tokenData);
      if (options.header) {
        options.header.Authorization = tokenData
      } else {
        const defaultHeader = {
          Authorization: tokenData
        }
        // 带上默认请求头
        options.header = Object.assign({}, defaultHeader, options.header)
      }
      // console.log(options.header, 'header');

      return options
    },
    // 响应拦截器 解构一层data
    response({ statusCode, data }) {
      // 后续补充实际逻辑
      //状态码为401
      if (statusCode === 401) {
        //获取页面栈
        const pageStack = getCurrentPages()
        //当前页面栈
        const currentPage = pageStack[pageStack.length - 1]
        const redirectUrl = currentPage.$page.fullPath || '/pages/task/index'
        const isTabbar = tabbarPagePaths.some((v) => v.startsWith(redirectUrl))
        const redirectType = isTabbar ? 'switchTab' : 'redirectTo'
        return uni.redirectTo({
          url: `/pages/login/index?redirectUrl=${encodeURIComponent(redirectUrl)}&redirectType=${redirectType}`
        })
      }
      return data
    },
  },
})
