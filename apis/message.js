import { uniFetch } from './uni-fetch.js'

//分页查询消息列表
export const getMessageListApi = (data) => {
  return uniFetch({
    url: '/driver/messages/page',
    method: 'GET',
    data: data
  })
}

//全部已读
export const readAllMessageApi = (contentType) => {
  return uniFetch({
    url: `/driver/messages/readAll/${contentType}`,
    method: 'PUT'
  })
}

//已读单个
export const readMessageApi = (id) => {
  return uniFetch({
    url: `/driver/messages/${id}`,
    method: 'PUT',
  })
}
