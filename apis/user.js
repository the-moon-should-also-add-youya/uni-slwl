import { uniFetch } from './uni-fetch.js'
//账号密码登录
export const loginApi = (data) => {
  return uniFetch({
    url: '/driver/login/account',
    method: 'POST',
    data: data
  })
}
//获取用户个人信息
export const getProfileApi = () => {
  return uniFetch({
    url: '/driver/users',
    method: 'GET',
  })
}
//获取任务信息
export const getTaskInfoApi = (year, month) => {
  return uniFetch({
    url: '/driver/users/taskReport',
    method: 'GET',
    data: {
      year,
      month
    }
  })
}
//获取车辆信息
export const getTruckInfoApi = () => {
  return uniFetch({
    url: '/driver/users/truck',
    method: 'GET',
  })
}
