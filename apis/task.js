import { uniFetch } from './uni-fetch.js'

//回车登记
export const truckRegistrationApi = (data) => {
  return uniFetch({
    url: '/driver/tasks/truckRegistration',
    method: 'POST',
    data: data
  })
}
//交付
export const deliverApi = (data) => {
  return uniFetch({
    url: '/driver/tasks/deliver',
    method: 'POST',
    data: data
  })
}
//上报异常
export const reportExceptionApi = (data) => {
  return uniFetch({
    url: '/driver/tasks/reportException',
    method: 'POST',
    data: data
  })
}

//任务列表
export const getTaskListApi = (params) => {
  return uniFetch({
    url: '/driver/tasks/list',
    method: 'GET',
    data: params
  })
}

//获取任务详情
export const getTaskDetailApi = (jobId) => {
  return uniFetch({
    url: `/driver/tasks/details/${jobId}`,
    method: 'GET',
  })
}

//延迟提货
export const delayApi = (data) => {
  return uniFetch({
    url: '/driver/tasks/delay',
    method: "PUT",
    data: data
  })
}

//提货
export const pickUpApi = (data) => {
  return uniFetch({
    url: '/driver/tasks/takeDelivery',
    method: 'POST',
    data: data
  })
}
