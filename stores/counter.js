//引入definesotre
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useCounterStore = defineStore('counter', () => {
  //数据
  const count = ref(0)
  //加
  const increment = () => {
    count.value++
  }
  //减
  const decrement = () => {
    count.value--
  }
  //记得return出去
  return { count, increment, decrement }
}, {
  persist: {
    paths: ['count']
  }
})