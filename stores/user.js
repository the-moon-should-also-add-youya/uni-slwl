import { defineStore } from 'pinia'
import { ref } from 'vue'
import { userKey } from '@/apis/uni-fetch.js'
export const useUserStore = defineStore('user', () => {
  const token = ref('')
  const setToken = (newToken) => {
    token.value = newToken
  }
  const redirectUrl = ref('/pages/task/index')
  const setRedirectUrl = (newUrl) => {
    redirectUrl.value = '/' + newUrl
  }
  const redirectType = ref('switchTab')
  const setRedirectType = (newType) => {
    redirectType.value = newType
  }

  return {
    token,
    setToken,
    redirectUrl,
    setRedirectUrl,
    redirectType,
    setRedirectType
  }
}, {
  persist: {
    paths: ['token'],
  }
})
